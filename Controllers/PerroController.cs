using System;
using System.Collections.Generic;
using System.Linq;
using EjemploAdministracionPerrosApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace EjemploAdministracionPerrosApi.Controllers
{
    [Route("api/[controller]")]
    public class PerroController : Controller
    {
        private readonly EjemploAdministracionPerrosContext context;
        
        public PerroController(EjemploAdministracionPerrosContext context)
        {
            this.context = context;
            
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Perro> Get()
        {
            
            return context.Perro.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Perro Get(int id)
        {
            return context.Perro.Find(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Perro value)
        {
            context.Perro.Add(value);
            context.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Perro value)
        {
            var old  = context.Perro.Find(id);
            old.PrrNombre = value.PrrNombre;
            old.PrrFechaModificacion = DateTime.Now; 
            old.RazId = value.RazId;
            context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var old  = context.Perro.Find(id);
            context.Perro.Remove(old);
            context.SaveChanges();
        }

    }
}