﻿using System;
using System.Collections.Generic;

namespace EjemploAdministracionPerrosApi.Model
{
    public partial class Perro
    {
        public int PrrId { get; set; }
        public string PrrNombre { get; set; }
        public int RazId { get; set; }
        public DateTime PrrFechaRegistro { get; set; }
        public DateTime? PrrFechaModificacion { get; set; }
        public int UsuIdRegistro { get; set; }
        public int? UsuIdModificacion { get; set; }
        public bool PrrActivo { get; set; }

        public virtual Raza Raz { get; set; }
        public virtual Usuario UsuIdModificacionNavigation { get; set; }
        public virtual Usuario UsuIdRegistroNavigation { get; set; }
    }
}
