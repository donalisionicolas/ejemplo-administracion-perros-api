namespace EjemploAdministracionPerrosApi.ViewModel.Login
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        
    }
}