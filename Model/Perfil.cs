﻿using System;
using System.Collections.Generic;

namespace EjemploAdministracionPerrosApi.Model
{
    public partial class Perfil
    {
        public Perfil()
        {
            Usuario = new HashSet<Usuario>();
        }

        public int PerId { get; set; }
        public string PerNombre { get; set; }
        public bool PerActivo { get; set; }

        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
